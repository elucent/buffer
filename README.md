# Buffer

Buffer is a simple C++ library that adds a basic byte buffer. The buffer
generally acts like a queue, but stores all data in a raw byte array.

It's not type-safe, but that's also not really the point - generally, Buffer is
intended to provide a simple means of storing plain data in a single data
structure, even across multiple types.

Example use:
```cpp
#include  <iostream>

// Including the library.
#include "Buffer.h"

struct Vec3 { float x, y, z; };

int main() {
    // Declaring a Buffer variable.
    Buffer buf;
    
    // Adding three pieces of data.
    buf << 1 << 2.0 << 3.f;

    // Adding a compound data type.
    buf.push(Vec3{ 1, 2, 3 });
    
    int a;
    double b;
    float c;
    
    // Deserialize data in the same order.
    buf >> a >> b >> c;
    Vec3 vec = buf.read<Vec3>();
    
    // Reset buffer, allowing it to be read again.
    buf.reset();
    
    return 0;
}