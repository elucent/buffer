#include "Buffer.h"

namespace buf {
    void Buffer::copy(void* dest, void* src, unsigned long int size) {
        uint8_t extra = size & 0b111;
        unsigned long int words = size >> 3;
        for (unsigned long int i = 0; i < words; i ++) *((uint64_t*)dest + i) = *((uint64_t*)src + i);
        for (unsigned long int i = size - extra; i < size; i ++) *((uint8_t*)dest + i) = *((uint8_t*)src + i);
    }

    void Buffer::grow() {
        uint8_t* old = buf;
        unsigned long int diff = ptr - old;
        buf = new uint8_t[capacity *= 2];
        copy(buf, old, sz);
        ptr = buf + diff;
        delete[] old;
    }

    void Buffer::shrink() {
        uint8_t* old = buf;
        buf = new uint8_t[capacity /= 2];
        copy(buf, ptr, capacity);
        ptr = buf;
    }

    Buffer::Buffer() 
        : buf(new uint8_t[8]), ptr(buf), capacity(8), sz(0) {
        //
    }

    Buffer::Buffer(const Buffer& other) 
        : buf(new uint8_t[other.capacity]), ptr(buf + (other.ptr - other.buf)), 
        capacity(other.capacity), sz(other.sz) {
        copy(buf, other.buf, other.capacity);
    }

    Buffer& Buffer::operator=(const Buffer& other) {
        if (this != &other) {
            delete[] buf;
            capacity = other.capacity;
            sz = other.sz;
            buf = new uint8_t[capacity];
            ptr = buf + (other.ptr - other.buf);
            copy(buf, other.buf, other.capacity);
        }
        return *this;
    }

    Buffer::~Buffer() {
        delete[] buf;
    }

    unsigned long int Buffer::size() const {
        return sz;
    }

    unsigned long int Buffer::length() const {
        return (buf + sz) - ptr;
    }

    void Buffer::reset() {
        ptr = buf;
    }

    uint8_t* Buffer::data() {
        return buf;
    }

    const uint8_t* Buffer::data() const {
        return buf;
    }

    Buffer::operator bool() const {
        return length();   
    }
}