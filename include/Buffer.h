#ifndef BUFFER_H
#define BUFFER_H

#include <cstdint>

namespace buf {
    using namespace std;

    class Buffer {
        uint8_t *buf, *ptr;
        unsigned long int capacity, sz;

        static void copy(void* dest, void* src, unsigned long int size);
        void grow();
        void shrink();
    public:
        Buffer();
        Buffer(const Buffer& other);
        Buffer& operator=(const Buffer& other);
        ~Buffer();

        void reset();
        unsigned long int size() const;
        unsigned long int length() const;
        uint8_t* data();
        const uint8_t* data() const;
        operator bool() const;

        template<typename T>
        void push(const T& t) {
            if (sz + sizeof(T) > capacity) grow();
            *(T*)(buf + sz) = t;
            sz += sizeof(T);
        }

        template<typename T, typename ...Args>
        void push(const T& t, Args... args) {
            push(t);
            push(args...);
        }

        template<typename T>
        T pop() {
            T t = *(T*)ptr;
            ptr += sizeof(T);
            sz -= sizeof(T);
            if (capacity > 8 && sz < capacity >> 2) shrink();
            return t;
        }

        template<typename T>
        Buffer& operator<<(const T& t) {
            push(t);
            return *this;
        }

        template<typename T>
        Buffer& operator>>(T& t) {
            t = read<T>();
            return *this;
        }

        template<typename T>
        T read() {
            T t = *(T*)ptr;
            ptr += sizeof(T);
            return t;
        }

        template<typename T>
        T& front() {
            return *(T*)ptr;
        }

        template<typename T>
        const T& front() const {
            return *(T*)ptr;
        }

        template<typename T>
        T& back() {
            return *(T*)((buf + sz) - sizeof(T));
        }

        template<typename T>
        const T& back() const {
            return *(T*)((buf + sz) - sizeof(T));
        }

        template<typename T>
        T& front(unsigned long int offset) {
            return *(T*)(ptr + offset);
        }

        template<typename T>
        const T& front(unsigned long int offset) const {
            return *(T*)(ptr + offset);
        }

        template<typename T>
        T& back(unsigned long int offset) {
            return *(T*)(((buf + sz) - offset) - sizeof(T));
        }

        template<typename T>
        const T& back(unsigned long int offset) const {
            return *(T*)(((buf + sz) - offset) - sizeof(T));
        }
    };
}

#endif